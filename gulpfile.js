'use strict';
const gulp = require('gulp');
const kraken = require('gulp-kraken');
const jimp = require('gulp-jimp');

gulp.task('kraken', () => {
  // specify here path to your images
  // more info about kraken api -> https://kraken.io/docs
  return gulp.src('src/assets/images/blue-banner*.jpg').pipe(
    kraken({
      key: 'bede939a0877097ff6a03ecf6f3c3645',
      secret: '406de655a1525b8b5747fa50c88327f25c132d0b',
      lossy: true,
      concurrency: 6,
    })
  );
});
 
const retina = false;
const imagesSrc = './src/assets/images/blue-banner.jpg';
const imagesFolder = './src/assets/images/';

let imageSizes = {
  '-320x1031': {
    cover: { width: 320, height: 1031 }
  },
  '-768x1031': {
    cover: { width: 768, height: 1031 }
  },
  '-768x878': {
    cover: { width: 768, height: 878 }
  },
  '-992x878': {
    cover: { width: 992, height: 878 }
  },
  '-1024x755': {
    cover: { width: 1024, height: 755 }
  },
  '-1200x755': {
    cover: { width: 1200, height: 755 }
  },
  '-1440x692': {
    cover: { width: 1440, height: 692 }
  },
  '-1680x692': {
    cover: { width: 1680, height: 692 }
  },
  '-1920x692': {
    cover: { width: 1920, height: 692 }
  },
  '-2560x692': {
    cover: { width: 2560, height: 692 }
  },
  '-3840x692': {
    cover: { width: 3840, height: 692 }
  }
};
const imageSizesRetina = {
  '-320x1031@2x': {
    cover: { width: 640, height: 2062 }
  },
  '-768x1031@2x': {
    cover: { width: 1536, height: 2062 }
  },
  '-768x878@2x': {
    cover: { width: 1536, height: 1756 }
  },
  '-992x878@2x': {
    cover: { width: 1984, height: 1756 }
  },
  '-1024x755@2x': {
    cover: { width: 2048, height: 1510 }
  },
  '-1200x755@2x': {
    cover: { width: 2400, height: 1510 }
  },
  '-1440x692@2x': {
    cover: { width: 2880, height: 1384 }
  },
  '-1680x692@2x': {
    cover: { width: 3360, height: 1384 }
  },
  '-1920x692@2x': {
    cover: { width: 3840, height: 1384 }
  },
  '-2560x692@2x': {
    cover: { width: 5120, height: 1384 }
  }
};

if (retina) {
  imageSizes = Object.assign({}, imageSizes, imageSizesRetina);
}

gulp.task('slice-images', function () {
  return gulp.src(imagesSrc)
          .pipe(jimp(imageSizes))
          .pipe(gulp.dest(imagesFolder));
});