import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  Renderer2,
  ViewChild,
  ElementRef,
  EventEmitter
} from '@angular/core';
 
import { FormGroup } from '@angular/forms';
import { Subject }    from 'rxjs';
  
/* Preventing build error () */
declare const safari: any;

@Component({
  selector: 'app-custom-input-file',
  templateUrl: './custom-input-file.component.html',
  styleUrls: ['./custom-input-file.component.less']
})
export class CustomInputFileComponent implements OnInit, OnDestroy {
  @Input('group') public userForm: FormGroup;
  @Input() parentSubject: Subject<any>;
  @Output() imageAdded = new EventEmitter<any>();

  isEmpty = false; // For preventing error message before user choose file or cancel input
  isPhotoRequired = false;
  maxFileSize: number = 5; // MB
  minImgRes: string = "70x70" // 70px width x 70px height
  allowedFormats: string[] = ['jpeg', 'jpg'];
  
  @ViewChild('inputCont')
  private inputCont: ElementRef;
  @ViewChild('inputEl')
  private inputEl: ElementRef;

  constructor(
    private renderer: Renderer2
  ) { }

  
  ngOnInit() {
    this.parentSubject.subscribe(event => {
      // Reset select value
      this.renderer.setProperty(this.inputCont.nativeElement, 'innerText', 'Upload your photo');
    });
  }

  ngOnDestroy() {
    // needed if child gets re-created (eg on some model changes)
    // note that subsequent subscriptions on the same subject will fail
    // so the parent has to re-create parentSubject on changes
    this.parentSubject.unsubscribe();
  }

  inputChanged(e): void {
    let fileName = e.currentTarget.value.split( '\\' ).pop() || 'Upload your photo';

    this.renderer.setProperty(this.inputCont.nativeElement, 'innerText', fileName);

    /* Angular validation for IOS */
    const iDevices = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ];

    const isSafari = (function(p) { return p.toString() === '[object SafariRemoteNotification]'; })( !window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification), );
    
    if (isSafari) {
      this.validateFile(e);
    } else if (!!navigator.platform) {
      while (iDevices.length) {
        if (navigator.platform === iDevices.pop()) {
          this.validateFile(e);

          break;
        }
      }
    }
    /* END Angular validation for IOS and safari */
    
    if (e.currentTarget.files[0]) {
      this.imageAdded.emit(e.currentTarget.files[0])
      this.renderer.removeClass(this.inputCont.nativeElement, 'empty-input');
      this.isEmpty = false;
    } else {
      this.imageAdded.emit('')
      this.renderer.addClass(this.inputCont.nativeElement, 'empty-input');
      this.isEmpty = true;
    }
    
    e.currentTarget.blur();
  }

  /* Only if default validation does not support (ios, Mac safari have issues with input type file validation) */
  private validateFile(e):void {
    let file = e.currentTarget.files[0];

    this.userForm.get('photo').markAsDirty();
    this.userForm.get('photo').markAsTouched();

    /* Remove required validator */
    this.userForm.get('photo').clearValidators();
    this.userForm.get('photo').clearAsyncValidators();
    this.userForm.get('photo').setErrors({});
    this.userForm.get('photo').updateValueAndValidity();

    /* Format validation */
    let forbiddenFormat = false;
    let allowedFormats = ['jpg', 'jpeg'];
    let type;
    let format;
    if (file) {
      type = file.type.split('/')[0];
      format = file.type.split('/')[1];
      
      if (type == 'image') {
        if (allowedFormats.indexOf(format) == -1) {
          forbiddenFormat = true;
        }
      } else {
        forbiddenFormat = true;
      }
    }

    /* maxSizeLimit validation */
    let maxFileSize = 5 * 1024 * 1024;
    let overLimit = false;
    if (file) {
      overLimit = maxFileSize < file.size;
    }


    let errors = {};
    if (forbiddenFormat) {
      errors['allowedFormat'] = true;
    }
    if (overLimit) {
      errors['maxFileSize'] = true;
    }

    if (forbiddenFormat || overLimit) {
      this.userForm.get('photo').setErrors(errors);
    }
  }
}