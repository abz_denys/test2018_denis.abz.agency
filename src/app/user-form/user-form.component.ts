import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject }    from 'rxjs';

import { UsersService } from '../users/users.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.less']
})
export class UserFormComponent implements OnInit {
  public positions: any[];
  public getPositionsError: boolean = false;
  public formSubmitting: boolean = false;
  public alertShow: boolean = false;
  public alertTitle: string;
  public alertText: string;
  public submitTimeout: any;
  public userForm = this.fb.group({ 
    name: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(60)
      ]
    ],
    email: [null, [
        Validators.required,
        Validators.pattern(/.[^\s]+@.[^\s]+\..[^\s]+/i),
        Validators.maxLength(100)
      ]
    ],
    phone: [null, [
        Validators.required,
        Validators.pattern(/0[0-9]{9}/)
      ]
    ],
    position_id: [null, [
        Validators.required,
        Validators.pattern(/[0-9]+/),
        Validators.min(1)
      ]
    ],
    photo: [null, [
        Validators.required
      ]
    ],
  });
  private imageToUpload: File;

  // Child components notification
  public parentSubject:Subject<any> = new Subject();

  @Input() usersComponent: any;
  
  @ViewChild('phoneInput')
  private phoneInput: ElementRef;

  constructor(
    private userService: UsersService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.getPositions();
  }

  getPositions():void {
    this.userService.getPositions()
      .subscribe(
        data => {
          this.positions = data.positions;
          if (!data.positions.length) {
            this.getPositionsError = true;
          }
        },
        error => {
          this.getPositionsError = true;
        }
      )
  }

  onImageAdded(file): void {
    this.imageToUpload = file;
  }

  submitForm():void {
    
    this.userForm.value.phone = this.userForm.value.phone.charAt(0) == '+' ? this.userForm.value.phone : '+38' + this.userForm.value.phone;
    const formValue = this.userForm.value;
    const formData = new FormData();

    formData.append('photo', this.imageToUpload);
    Object.keys(formValue).forEach(function (item) {
      if (item != 'photo') {        
        formData.append(item, formValue[item]);
      }
    });

    if (this.userForm.valid) {
      // this.metrika.fireEvent('sign_up');
      this.createUser(formData);
    }
  }

  createUser(userData): void {
    this.submitTimeout = setTimeout(() => {
      this.formSubmitting = true;
    }, 350);
    this.userService.getToken()
      .subscribe(
        data => {
          this.userService.createtUser(userData, data.token)
            .subscribe(
              data => {
                clearTimeout(this.submitTimeout);
                this.formSubmitting = false;
                
                this.showAlert(
                  'Congratulations',
                  'You have successfully passed the registration'
                );
                
                // reset form
                this.userForm.reset();
                // Notify custom components
                this.notifyChildren();
                // Update users block
                this.usersComponent.getUpdatedUsers();
              },
              error => {
                let message: string = '';
                
                clearTimeout(this.submitTimeout);
                this.formSubmitting = false;

                if (error.status == 409) {
                  message = error.error.message;
                } else if (error.status == 422) {
                  for (var prop in error.error.fails) {
                    message += error.error.fails[prop][0] + ' ';
                  }
                }

                this.showAlert(
                  'Error',
                  message
                );
              }
            )
        },
        error => {
          clearTimeout(this.submitTimeout);
          this.formSubmitting = false;
          
          this.showAlert(
            'Error',
            `Unfortunately, an error occurred!\n 
            Please, check the internet conection or try again later.`
          );
        }
      )
  }

  // Child components notification
  notifyChildren() {
    this.parentSubject.next('Form submitted');
  }

  closeAlert(e):void {
    e.preventDefault();
    
    this.alertShow = false;
  }

  onFocusPhone(e):void {
    
    let input = e.currentTarget;
    let phoneValue = this.userForm.get('phone').value;
    let valueLength;
    let cursorPosition;

    if (phoneValue) {
      valueLength = phoneValue.length;

      if (phoneValue.length > 3) {
        valueLength += 2;
      }
      if (phoneValue.length > 6) {
        valueLength++;
      }
      if (phoneValue.length > 8) {
        valueLength++;
      }
    } else {
      valueLength = 0;
    }

    cursorPosition = 5 + valueLength; // 5 chars includes +38 (

    setTimeout(function() {
      input.setSelectionRange(cursorPosition, cursorPosition);
    }, 50);

    if (this.phoneInput.nativeElement.classList.contains('empty-input')) {
      this.phoneInput.nativeElement.classList.remove('empty-input');
    }
  }

  onBlurPhone(): void {
    if (this.userForm.get('phone').value == '') {
      this.phoneInput.nativeElement.classList.add('empty-input');
    }
  }

  keyDown(e):void {
    const code = e.keyCode;
    const allowedKeyCode = [
      48, 49, 50, 51, 52, 53, 54, 55, 56, 57, /* numbers */ 
      96, 97, 98, 99, 100, 101, 102, 103, 104, 105, /* numbers */ 
      8, /* backspace */ 
      46, /* delete */ 
      13, /* Enter */
      9 /* Tab */];

    if (allowedKeyCode.indexOf(code) == -1) {
      e.preventDefault();
    }
    if (code == 8) {
      if (!this.userForm.get('phone').value) {
        e.preventDefault();
      }
    }
  }

  private showAlert(title, text) {
    this.alertTitle = title;
    this.alertText = text;
    this.alertShow = true;
  }
}
