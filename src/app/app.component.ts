import { Component, OnInit, Renderer2 } from '@angular/core';

/* Preventing build error () */
declare const InstallTrigger: any;
declare const safari: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'test2018denis';

  constructor(private renderer: Renderer2) {
  }

  ngOnInit() {
    this.appendSpecialclasses();

    const getRandomInt = (min, max) =>
    Math.floor(Math.random() * (max - min)) + min;
    const variations = ['orange_btn', 'green_btn'];
    const varId = getRandomInt(0,2);

    this.renderer.addClass(document.body, 'gtm_ab_test_' + variations[varId]);

    // @ts-ignore
    window.yaParams = {ab_test: variations[varId]};
  }

  appendSpecialclasses(): void {
    if (this.isMac()) {
      this.renderer.addClass(document.body, 'apply-mac-classes');
    }

    if (this.isWind()) {
      this.renderer.addClass(document.body, 'apply-wind-classes');
    }

    if (this.isIOS()) {
      this.renderer.addClass(document.body, 'apply-ios-classes');
    }
    
    if (this.isFirefox()) {
      this.renderer.addClass(document.body, 'apply-ff-classes');
    }

    if (this.isSafari()) {
      this.renderer.addClass(document.body, 'apply-safari-classes');
    }

    if (this.isChrome()) {
      this.renderer.addClass(document.body, 'chrome');
    }
  }

  isMac():boolean {
    return navigator.userAgent.search(/(Mac|Macintosh)/i) == -1 ? false : true;
  }

  isWind(): boolean {
    return navigator.appVersion.includes('Win');
  }

  isIOS():boolean {
    const iDevices = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ];
  
    if (!!navigator.platform) {
      while (iDevices.length) {
        if (navigator.platform === iDevices.pop()){ return true; }
      }
    }
  
    return false;
  }

  isChrome():boolean {
    // Chrome 1+
    // @ts-ignore
    return !!window.chrome;
  }

  isFirefox():boolean {
    return typeof InstallTrigger !== 'undefined';
  }

  isSafari():boolean {
    return (function(p) { return p.toString() === '[object SafariRemoteNotification]'; })( !window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification), );
  }
}
