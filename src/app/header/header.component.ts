import { Component, OnInit, HostListener } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  query,
  animateChild,
  group
} from '@angular/animations';

import { UsersService } from '../users/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  animations: [
    trigger('mobileMenuBackgroundAnimation', [
      state('open', style({
        backgroundColor: 'rgba(0, 0, 0, .35)',
        pointerEvents: 'auto'
      })),
      state('close', style({
        backgroundColor: 'rgba(0, 0, 0, 0)'
      })),
      transition('open => close', [
        group([
          animate('130ms ease-in'),
          query('@mobileMenuContainerAnimation', [
            animateChild()
          ]),
        ])
      ]),
      transition('close => open', [
        group([
          query('@mobileMenuContainerAnimation', [
            animateChild()
          ]),
          animate('330ms ease-out'),
        ])
      ])
    ]),
    trigger('mobileMenuContainerAnimation', [
      state('open', style({
        transform: 'translateX(0)'
      })),
      state('close', style({
        transform: 'translateX(-103%)'
      })),
      transition('open => close', [
        animate('130ms ease-in')
      ]),
      transition('close => open', [
        animate('330ms ease-out')
      ])
    ])
  ]
})
export class HeaderComponent implements OnInit {
  public mobileMenuIsOpen: boolean = false;
  public currentUser: any;
  public currentUserLoaded: boolean = false;
  public currentUserError: boolean = false;
  private screenWidth: number;
  private currentUserId: number = 1;

  constructor(
    private userService: UsersService
  ) { }

  ngOnInit() {

    this.getUser();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth >=  992 && this.mobileMenuIsOpen) {
      document.body.classList.remove('prevent-scroll');
      document.body.removeAttribute('style');
    }
    if (window.innerWidth <  991.98 && this.mobileMenuIsOpen) {
      document.body.classList.add('prevent-scroll');
    }
  }

  getUser(): void {
    this.userService.getUser(this.currentUserId)
    .subscribe(
      data => {
        this.currentUserLoaded = true;
        this.currentUser = data.user;
      },
      error => {
        this.currentUserError = true;
      }
    );
  }

  toggleMobileMenu(): void {
    
    let pagePosition: any = 0;

    if (!this.mobileMenuIsOpen) {
      pagePosition = window.pageYOffset || document.querySelector('div').scrollTop;
    }

    if (window.innerWidth < 991.98) {
      this.mobileMenuIsOpen = !this.mobileMenuIsOpen;
      document.body.classList.toggle('prevent-scroll');
    }

    if (pagePosition) {
      document.body.style.top = '-' + pagePosition + 'px';
    }
    
    if (!this.mobileMenuIsOpen) {
      let bodyStyle = document.body.getAttribute('style');

      if (bodyStyle) {
        pagePosition = document.body.getAttribute('style').match(/\d+/)[0];

        document.body.removeAttribute('style');
        window.scroll(0, pagePosition);
      }
      

    }

  }

  stopPropagation(e): void {
    e.stopPropagation();
  }
}
