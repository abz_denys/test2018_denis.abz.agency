import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule }    from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { UsersComponent } from './users/users.component';
import { UserPhotoDirective } from './users/user-photo.directive';
import { TooltipDirective } from './users/tooltip.directive';
import { UserFormComponent } from './user-form/user-form.component';
import { CustomInputFileComponent } from './user-form/custom-input-file/custom-input-file.component';
import { MaxFileSizeDirective } from './user-form/custom-input-file/max-file-size.directive';
import { MinImageResolutionDirective } from './user-form/custom-input-file/min-image-resolution.directive';
import { AllowedImageFormatsDirective } from './user-form/custom-input-file/allowed-image-formats.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    UsersComponent,
    UserPhotoDirective,
    TooltipDirective,
    UserFormComponent,
    CustomInputFileComponent,
    MaxFileSizeDirective,
    MinImageResolutionDirective,
    AllowedImageFormatsDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgSelectModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot({}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
