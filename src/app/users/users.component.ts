import { Component, OnInit, HostListener } from '@angular/core';

import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {
  private usersPerPage: number;
  private pageLoaded: number = 0;
  private resizeTimeout;
  private showMoreTimeout;
  private usersLoading: boolean = false;
  public users: any[] = [];
  public totalUsers: number = 0;
  public usersCount: any[] = [1, 1, 1, 1, 1, 1];
  public hideBlock: boolean = false;
  public usersToLoad: number;
  public showLoader: boolean = false;
  public btnColor: string;

  constructor(
    private userService: UsersService
  ) { }

  ngOnInit() {
    if (window.innerWidth < 768) {
      this.usersPerPage = 3;
      this.usersCount = [1, 1, 1];
    } else {
      this.usersPerPage = 6;
      this.usersCount = [1, 1, 1, 1, 1, 1];
    }
    
    this.getUsers();
  }

  getUsers():void {
    this.userService.getUsers(this.usersPerPage, ++this.pageLoaded)
      .subscribe(
        data => {

          data.users.forEach(element => {
            this.prettifyUserData(element);
          });

          this.users = data.users;
          this.totalUsers = data.total_users;
          this.usersToLoad = this.users.length - this.totalUsers;

          this.setUserPerPage();

          if (!data.users.length) {
            this.hideBlock = true;
          }
        },
        error => {
          this.hideBlock = true;
        }
      )
  };

  showMore(cb):void {
    if (!this.usersLoading) {
      this.usersLoading = true;
      this.showMoreTimeout = setTimeout(() => {
        this.showLoader = true;
      }, 350);
      this.userService.getUsers(this.usersPerPage, ++this.pageLoaded)
        .subscribe(data => {
          clearTimeout(this.showMoreTimeout);
          this.showLoader = false;
          this.usersLoading = false;
          
          data.users.forEach(element => {
            this.prettifyUserData(element);
          });
  
          this.users = this.users.concat(data.users);
          this.usersToLoad = this.users.length - this.totalUsers;
  
          if (cb) {
            cb();
          }
        })
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    clearTimeout(this.resizeTimeout);
    this.resizeTimeout = setTimeout(this.checkIfNeedToloadMoreUsers.bind(this), 350);
  }

  private setUserPerPage() {
    if (window.innerWidth < 768) {
      this.usersPerPage = 3;
    } else {
      this.usersPerPage = 6;
    }
  }
  
  private prettifyUserData(user) {
    /* Prevent line break in email */
    user.email = user.email.replace(/-/g, '\u2011');

    /* Add phone mask */
    let countryCode = user.phone.slice(0, 3);
    let operatorCode = user.phone.slice(3, 6);
    let telPart1 = user.phone.slice(6, 9);
    let telPart2 = user.phone.slice(9, 11);
    let telPart3 = user.phone.slice(11);

    user.phone =
      countryCode +
      " (" + operatorCode + ") " + 
      telPart1 + " " +
      telPart2 + " " +
      telPart3;
    
  }

  private checkIfNeedToloadMoreUsers() {
    this.setUserPerPage();

    if (this.users.length != this.totalUsers) {
      let pagesLoaded = this.users.length / this.usersPerPage;
  
      if (Number.isInteger(pagesLoaded)) {
        this.pageLoaded = pagesLoaded;
      } else {
        this.usersPerPage = 3;
  
        this.showMore(function() {
          this.usersPerPage = 6;
          this.pagesLoaded = pagesLoaded + 0.5;
        }.bind(this));
      }
    }
  }

  // Get users after form submitting
  getUpdatedUsers(): void {
    this.pageLoaded = 0;
    this.getUsers();
  }

  // возвращает cookie с именем name, если есть, если нет, то undefined
  private getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
}
