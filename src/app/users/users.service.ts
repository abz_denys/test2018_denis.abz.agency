import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Token': ''
  })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private usersUrl: string = 'https://frontend-test-assignment-api.abz.agency/api/v1/users';
  private positionsUrl: string = 'https://frontend-test-assignment-api.abz.agency/api/v1/positions';
  private tokenUrl: string = 'https://frontend-test-assignment-api.abz.agency/api/v1/token';

  constructor(
    private http: HttpClient
  ) { }

  getUsers (usersPerPage, pageToLoad): Observable<any> {
    const url = `${this.usersUrl}?page=${pageToLoad}&count=${usersPerPage}`;
    return this.http.get<any>(url);
  }
  
  getUser(id: number): Observable<any> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<any>(url);
  }

  getPositions (): Observable<any> {
    return this.http.get<any>(this.positionsUrl);
  }

  getToken (): Observable<any> {
    return this.http.get<any>(this.tokenUrl);
  }

  createtUser(userData: any, token: string): Observable<any> {
    httpOptions.headers =
      httpOptions.headers.set('Token', token);
    return this.http.post<any>(this.usersUrl, userData, httpOptions);
  }
}
