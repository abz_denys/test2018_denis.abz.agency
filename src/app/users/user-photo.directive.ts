import { Directive, ElementRef, OnInit, Input  } from '@angular/core';

@Directive({
  selector: '[appUserPhoto]'
})
export class UserPhotoDirective implements OnInit {
  @Input('appUserPhoto') photoSrc: string;

  constructor(
    private el: ElementRef
  ) { }

  ngOnInit() {
    let img = new Image();

    img.onload = function () {
      this.el.nativeElement.src = this.photoSrc;
        this.el.nativeElement.removeAttribute('style');
    }.bind(this);

    img.src = this.photoSrc;
  }
  
}
