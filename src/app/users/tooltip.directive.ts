import {
  Directive,
  ElementRef,
  OnDestroy,
  AfterViewInit,
  Renderer2
} from '@angular/core';
import { EventManager } from '@angular/platform-browser';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements AfterViewInit, OnDestroy {
  private _onMouseenter = this.onMouseenter.bind(this);
  private _onMouseleave = this.onMouseleave.bind(this);
  private _onResize = this.onResize.bind(this);
  private tooltip: any;
  private tooltipText: any;
  private resizeTimeout: any;

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private eventManager: EventManager
  ) { }

  ngAfterViewInit() {
    let offsetWidth: number = this.el.nativeElement.offsetWidth;

    // For Edge
    // @ts-ignore
    if (!!window.StyleMedia) {
      offsetWidth++;
    }
    
    if (this.el.nativeElement.scrollWidth > offsetWidth) {
      const elWrapper = this.el.nativeElement.parentNode;

      this.el.nativeElement.addEventListener('mouseenter', this._onMouseenter);
      this.el.nativeElement.addEventListener('mouseleave', this._onMouseleave);
      this.eventManager.addGlobalEventListener('window', 'resize', this._onResize);

      this.tooltip = this.renderer.createElement('div');
      this.tooltipText = this.renderer.createText(this.el.nativeElement.innerText);
      this.renderer.addClass(this.tooltip, 'abz-tooltip');
      this.renderer.setStyle(elWrapper, 'position', 'relative');
      this.renderer.setStyle(elWrapper, 'cursor', 'pointer');
      this.renderer.appendChild(this.tooltip, this.tooltipText);
      this.renderer.appendChild(elWrapper, this.tooltip);

      /* Set tooltip position */
      this.onResize();
    }
  }
  
  ngOnDestroy() {
    this.el.nativeElement.removeEventListener('mouseenter', this._onMouseenter);
    this.el.nativeElement.removeEventListener('mouseleave', this._onMouseleave);
  }

  onMouseenter(e) {
    this.renderer.addClass(this.tooltip, 'visible');
  }
  
  onMouseleave(e) {
    this.renderer.removeClass(this.tooltip, 'visible');
  }

  onResize() {
    let blockCenter = this.el.nativeElement.clientWidth / 2;
    let tooltipCenter = this.tooltip.scrollWidth / 2;

    this.renderer.removeStyle(this.tooltip, 'left');
    this.renderer.removeStyle(this.tooltip, 'right');
    
    if (blockCenter > tooltipCenter) {
      this.renderer.setStyle(this.tooltip, 'left', blockCenter - tooltipCenter + 'px');
    } else {
      this.renderer.setStyle(this.tooltip, 'right', '0');
      this.renderer.setStyle(this.tooltip, 'max-width', '100%');
      this.renderer.setStyle(this.tooltip, 'word-break', 'break-all');
    }
  }
}
